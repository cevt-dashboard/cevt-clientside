import React, { Component } from "react";
import { connect } from "react-redux";

class Profile extends Component {
  render() {
    return (
      <>
        <div className="main-body">
          {/* /Breadcrumb */}
          <div className="row gutters-sm">
            <div className="col-md-4 mb-3">
              <div className="card">
                <div className="card-body">
                  <div className="d-flex flex-column align-items-center text-center">
                    <img
                      src={"avatars/profile.png"}
                      alt="Admin"
                      className="rounded-circle"
                      width={150}
                    />
                    <div className="mt-3">
                      <h4>
                        {this.props.user.firstName.charAt(0).toUpperCase() +
                          this.props.user.firstName.slice(1) +
                          " " +
                          this.props.user.lastName.toUpperCase()}
                      </h4>
                      <p className="text-secondary mb-1">
                        {this.props.user.email}
                      </p>
                      <p className="text-secondary mb-1">
                        Université d'Evry Val d'Essonne
                      </p>
                      <p className="text-muted font-size-sm">IBGBI</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-8">
              <div className="card mb-3">
                <div className="card-body">
                  <div className="row">
                    <div className="col-sm-3">
                      <h6 className="mb-0">Prénom</h6>
                    </div>
                    <div className="col-sm-9 text-secondary">
                      {this.props.user.firstName.charAt(0).toUpperCase() +
                        this.props.user.firstName.slice(1)}
                    </div>
                  </div>
                  <hr />
                  <div className="row">
                    <div className="col-sm-3">
                      <h6 className="mb-0">Nom</h6>
                    </div>
                    <div className="col-sm-9 text-secondary">
                      {this.props.user.lastName.toUpperCase()}
                    </div>
                  </div>
                  <hr />
                  <div className="row">
                    <div className="col-sm-3">
                      <h6 className="mb-0">Email</h6>
                    </div>
                    <div className="col-sm-9 text-secondary">
                      {this.props.user.email}
                    </div>
                  </div>
                  <hr />
                  <div className="row">
                    <div className="col-sm-3">
                      <h6 className="mb-0">Numéro de tel</h6>
                    </div>
                    <div className="col-sm-9 text-secondary">Non renseigné</div>
                  </div>
                  <hr />
                  <div className="row">
                    <div className="col-sm-3">
                      <h6 className="mb-0">Rôle</h6>
                    </div>
                    <div className="col-sm-9 text-secondary">
                      {this.props.user.roles.includes("ROLE_ADMIN")
                        ? "Administrateur"
                        : "Enseignant"}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.authentication.user,
  };
};
export default connect(mapStateToProps)(Profile);
