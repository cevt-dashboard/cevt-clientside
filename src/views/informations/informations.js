import React from "react";
import { CCard, CCardHeader, CCardBody } from "@coreui/react";

const informations = () => {
  return (
    <>
      <CCard>
        <CCardHeader>Dashboard</CCardHeader>
        <CCardBody>
          <div className="media">
            <div className="media-left">
              <img
                className="media-object mr-5"
                src="imgs/business-report.png"
                width="100px"
              />
            </div>
            <div className="media-body">
              <h4 className="media-heading">Tableau de bord</h4>
              La page Dashboard est la page principale, sur cette page vous
              trouverez des indicateurs qui concernent le suivi de la charge
              d'enseignement comme :<br />
              - La moyenne des heures pour chaque catégorie et le pourcentage du
              travail effectué.
              <br />
              - Le nombre des enseignants dans enregistrés.
              <br />- L'evolution de la moyenne d'heures d'enseignements par
              mois <br />
            </div>
          </div>
        </CCardBody>
      </CCard>
      <CCard>
        <CCardHeader>Calendrier</CCardHeader>
        <CCardBody>
          <div className="media">
            <div className="media-left">
              <img
                className="media-object mr-5"
                src="imgs/schedule.png"
                width="100px"
              />
            </div>
            <div className="media-body">
              <h4 className="media-heading">Calendrier des enseignants</h4>
              Le calendrier des enseignants s'agit d'un agenda, sur lequel les
              enseignants peuvent consulter les séances d'enseignements prévus,
              ainsi que faire des demandes de résérvations d'événements. Cette
              page permet à les enseignants aussi de séléctionner une séance et
              débuter un appel d'absences numérique.
            </div>
          </div>
        </CCardBody>
      </CCard>
      <CCard>
        <CCardHeader>Statistiques</CCardHeader>
        <CCardBody>
          <div className="media">
            <div className="media-left">
              <img
                className="media-object mr-5"
                src="imgs/analytics.png"
                width="100px"
              />
            </div>
            <div className="media-body">
              <h4 className="media-heading">
                Statistiques pour le suivi de la charge d'enseignement
              </h4>
              La page statistiques est une page conçu pour une utilisation plus
              avancée de l'outil CEVT Dashboard. <br />
              Vous trouverez tous les éléments requis pour une analyse sur la
              charge d'enseignements pour l'ensemble des enseignants. <br />
              Des indicateurs, graphiques et tableaux sont mis à votre
              disposition pour vous faciliter l'analyse et le suivi de la charge
              d'enseignement.
            </div>
          </div>
        </CCardBody>
      </CCard>
      <CCard>
        <CCardHeader>Profile</CCardHeader>
        <CCardBody>
          <div className="media">
            <div className="media-left">
              <img
                className="media-object mr-5"
                src="imgs/profile.png"
                width="100px"
              />
            </div>
            <div className="media-body">
              <h4 className="media-heading">Profile de l'enseignant</h4>
              Cette page vous permet de consulter votre profile et voir vos
              données personnelles.
              <br />
              N'hésitez pas à nous contacter via notre adresse mail si vous
              voyez des erreurs dans votre fiche d'informations.
            </div>
          </div>
        </CCardBody>
      </CCard>
    </>
  );
};

export default informations;
