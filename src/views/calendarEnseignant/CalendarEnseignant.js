import React from 'react'

 
import Calendar from '../calendar/Calendar'

const CalendarEnseignant = () => {
  
  return (
    <div className="parentDiv">
     <Calendar className="mt-2 childDiv" isTeacher={true}  />
     <br></br>
     <br></br>
    </div>

  )
}

export default CalendarEnseignant


