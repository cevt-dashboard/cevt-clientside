import React, { Component } from "react";
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CProgress,
  CRow,
  CWidgetProgress,
  CDataTable,
} from "@coreui/react";

import MainChartExample from "../charts/MainChartExample.js";
import * as Constants from "../../_services/constants";

import {
  getAverages,
  getAllTeacherDetails,
  getMonthlyTotal,
} from "../../_services/teachers.service";
import { CSVLink } from "react-csv";
class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.supportedLanguage = Constants.FR;
    this.isLoading = true;
    this.state = {
      averages: {},
      totals: {},
      teachersList: {},
      dataTableData: [],
    };
  }

  async componentDidMount() {
    this.initData();
  }
  initData() {
    getAverages().then((averages) => {
      getAllTeacherDetails().then((teachers) => {
        getMonthlyTotal().then((monthlyTotal) => {
          var cleanedData = this.getAveragesFromData(averages);
          var dataTable = [];
          teachers.forEach((x) => {
            if (
              x.hasOwnProperty("Done") &&
              !(((x.Total.cm === x.Total.td) === x.Total.tp) === 0)
            ) {
              dataTable.push({
                calName: x.Stats.calName,
                cm: x.Done.cm,
                td: x.Done.td,
                tp: x.Done.tp,
                cmTotal: x.Total.cm,
                tdTotal: x.Total.td,
                tpTotal: x.Total.tp,
              });
            }
          });
          this.isLoading = false;
          this.setState({
            dataTableData: dataTable,
            averages: cleanedData.averages,
            totals: cleanedData.totals,
            monthlyTotal: monthlyTotal,
            //numberTeachers: monthlyAvg.dataProviders.length,
            //monthlyAverages : this.getMonthlyData(monthlyAvg.map, monthlyAvg.dataProviders.length)
          });
        });
      });
    });
  }
  getMonthlyData = (data, nbTeachers) => {
    var result = [];

    Object.entries(data).forEach(([key, value]) => {
      result.push({
        date: new Date(
          key.toString().substr(0, 4),
          key.toString().substr(4, 5) - 1,
          27
        ).toLocaleDateString("fr-FR"),
        avgCM: value.cmTotal / nbTeachers,
        avgTD: value.tdTotal / nbTeachers,
        avgTP: value.tpTotal / nbTeachers,
      });
    });
    return result;
  };

  getAveragesFromData = (data) => {
    return {
      averages: {
        avgCM: data.averages
          .filter((item) => item.label === Constants.CM)[0]
          .average.toFixed(2),
        avgTD: data.averages
          .filter((item) => item.label === Constants.TP)[0]
          .average.toFixed(2),
        avgTP: data.averages
          .filter((item) => item.label === Constants.TD)[0]
          .average.toFixed(2),
      },
      totals: {
        totalCM: data.averages
          .filter((item) => item.label === Constants.CM)[0]
          .total.toFixed(2),
        totalTD: data.averages
          .filter((item) => item.label === Constants.TP)[0]
          .total.toFixed(2),
        totalTP: data.averages
          .filter((item) => item.label === Constants.TD)[0]
          .total.toFixed(2),
      },
    };
  };

  getPercentage = (type) => {
    var res = { done: 0, total: 0 };
    switch (type) {
      case "CM":
        this.state.dataTableData.forEach((x) => {
          res.done += x.cm;
          res.total += x.cmTotal;
        });
        break;
      case "TP":
        this.state.dataTableData.forEach((x) => {
          res.done += x.tp;
          res.total += x.tpTotal;
        });
        break;
      case "TD":
        this.state.dataTableData.forEach((x) => {
          res.done += x.td;
          res.total += x.tdTotal;
        });
        break;
      default:
    }
    return ((res.done / res.total) * 100).toFixed(2);
  };
  getData() {
    return {
      data: [...this.state.dataTableData],
      headers: [
        { label: "Nom Enseignant", key: "calName" },
        { label: "Cm Effectue", key: "cm" },
        { label: "Td Effectue", key: "td" },
        { label: "Tp Effectue", key: "tp" },
        { label: "Cm Total", key: "cmTotal" },
        { label: "Td Total", key: "tdTotal" },
        { label: "Tp Total", key: "tpTotal" },
      ],
      filename: "export_moyenne_avancement_enseignants.csv",
    };
  }
  render() {
    return (
      <>
        {this.isLoading === false ? (
          <>
            <CRow>
              <CCol xs="12" sm="6" lg="3">
                <CWidgetProgress
                  header={this.state.averages.avgCM}
                  text="Moyenne des heures CM"
                  footer={
                    this.getPercentage("CM") +
                    "% des séances CM ont été éffectués."
                  }
                >
                  <CProgress
                    color="info"
                    size="xs"
                    className="my-3"
                    value={this.getPercentage("CM")}
                  />
                </CWidgetProgress>
              </CCol>
              <CCol xs="12" sm="6" lg="3">
                <CWidgetProgress
                  header={this.state.averages.avgTD}
                  text="Moyenne des heures TD"
                  footer={
                    this.getPercentage("TD") +
                    "% des séances TD ont été éffectués."
                  }
                >
                  <CProgress
                    color="info"
                    size="xs"
                    className="my-3"
                    value={this.getPercentage("TD")}
                  />
                </CWidgetProgress>
              </CCol>
              <CCol xs="12" sm="6" lg="3">
                <CWidgetProgress
                  header={this.state.averages.avgTP}
                  text="Moyenne des heures TP"
                  footer={
                    this.getPercentage("TP") +
                    "% des séances TP ont été éffectués."
                  }
                >
                  <CProgress
                    color="info"
                    size="xs"
                    className="my-3"
                    value={this.getPercentage("TP")}
                  />
                </CWidgetProgress>
              </CCol>
              <CCol xs="12" sm="6" lg="3">
                <CWidgetProgress
                  header={this.state.monthlyTotal.dataProviders.length.toString()}
                  text="Nombre des enseignants"
                  footer="Nombre des profs actifs"
                >
                  <CProgress
                    color="info"
                    size="xs"
                    className="my-3"
                    value={100}
                  />
                </CWidgetProgress>
              </CCol>
            </CRow>
            <CCard>
              <CCardBody>
                <CRow>
                  <CCol sm="12">
                    <h4 id="traffic" className="card-title mb-0">
                      {" "}
                      {Constants.avgEvolutionTitle.get(
                        this.supportedLanguage
                      )}{" "}
                    </h4>
                    <div className="small text-muted">
                      {new Date().toLocaleDateString("fr")}
                    </div>
                  </CCol>
                </CRow>
                <MainChartExample
                  style={{ height: "300px", marginTop: "40px" }}
                  dataset={this.getMonthlyData(
                    this.state.monthlyTotal.map,
                    this.state.monthlyTotal.dataProviders.length
                  )}
                />
              </CCardBody>
            </CCard>

            <CCard>
              <CCardHeader>
                Liste des enseignants
                <CSVLink {...this.getData()} style={{ float: "right" }}>
                  Csv
                </CSVLink>
              </CCardHeader>
              <CCardBody>
                <CDataTable
                  items={this.state.dataTableData}
                  fields={Constants.DashboardFields}
                  columnFilter
                  tableFilter
                  itemsPerPageSelect
                  itemsPerPage={5}
                  hover
                  sorter
                  pagination
                  striped={true}
                  responsive={true}
                />
              </CCardBody>
            </CCard>
          </>
        ) : (
          <div className="text-center">
            <img
              src="https://i.stack.imgur.com/hzk6C.gif"
              alt="Loading"
              style={{
                position: "absolute",
                top: "30%",
                left: "45%",
                width: "300px",
                height: "200px",
              }}
            />
          </div>
        )}
      </>
    );
  }
}

export default Dashboard;
