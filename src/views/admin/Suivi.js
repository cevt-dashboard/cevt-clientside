import React, { useEffect, useState } from "react";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CDataTable,
  CDropdown,
  CDropdownToggle,
  CDropdownMenu,
  CDropdownItem,
} from "@coreui/react";
import * as Constants from "../../_services/constants";
import Moment from "moment";
import * as Service from "../../_services/admin.service";

const Suivi = () => {
  const [demandesOriginal, setDemandesOriginal] = useState([]);
  const [demandes, setDemandes] = useState([]);

  useEffect(() => {
    getData();
  }, []);

  const getData = (e) => {
    const promises = [];
    promises.push(Service.getPendingEvents("NO"));
    promises.push(Service.getPendingEvents("YES"));
    promises.push(Service.getPendingEvents("REFUSED"));

    Promise.all(promises).then((result) => {
      var data = [...result[2], ...result[1], ...result[0]];
      setDemandesOriginal(data);
      setDemandes(adaptTable(data));
    });
  };

  const adaptTable = (items) => {
    var arrayResult = [];
    items.forEach((item) => {
      var demande = {
        id: item.id,
        name: item.summary,
        prof: item.prof,
        date_debut: Moment(item.dtStart).format("DD MM YYYY hh:mma"),
        duree:
          Moment.duration(item.duree).hours() +
          "h" +
          Moment.duration(item.duree).minutes() +
          "m",
        type: item.type,
        location: item.location,
        status: item.valid,
      };
      arrayResult.push(demande);
    });
    return arrayResult;
  };

  const approuverDemande = (e) => {
    var x = demandesOriginal.find((i) => i.id === e.id);
    setDemandes(demandes);
    x.valid = "YES";

    Service.updateEvents(x).then(() => {
      window.alert("La demande a été approuvée avec succès.");
      window.location.reload();
      demandes.map((i) => {
        if (i.id === e.id) {
          i.valid = "YES";
        }
        return i;
      });
      setDemandes(demandes);
    });
  };

  const rejeterDemande = (e) => {
    var x = demandesOriginal.find((i) => i.id === e.id);
    x.valid = "REFUSED";

    Service.updateEvents(x).then(() => {
      window.alert("La demande a été refusée.");
      window.location.reload();
    });
  };

  const getBadge = (status) => {
    switch (status) {
      case "YES":
        return "success";
      case "NO":
        return "warning";
      case "REFUSED":
        return "danger";
      default:
        return "secondary";
    }
  };

  const getStatus = (status) => {
    switch (status) {
      case "YES":
        return "Approuvé";
      case "NO":
        return "En attente";
      case "REFUSED":
        return "Refusé";
      default:
        return "Erreur";
    }
  };

  return (
    <>
      <CRow>
        <CCol xs="12" sm="12">
          <CCard>
            <CCardHeader>Suivi des demandes</CCardHeader>
            <CCardBody>
              <CDataTable
                items={demandes}
                fields={Constants.AdminFields}
                columnFilter
                tableFilter
                itemsPerPageSelect
                itemsPerPage={10}
                hover
                sorter
                pagination
                scopedSlots={{
                  actions: (item) => {
                    return (
                      <td className="py-2">
                        <CDropdown className="mt-2">
                          <CDropdownToggle caret color="info">
                            Action
                          </CDropdownToggle>
                          <CDropdownMenu>
                            <CDropdownItem
                              onClick={(e) => approuverDemande(item)}
                            >
                              Approuver
                            </CDropdownItem>
                            <CDropdownItem
                              onClick={(e) => rejeterDemande(item)}
                            >
                              Rejeter
                            </CDropdownItem>
                          </CDropdownMenu>
                        </CDropdown>
                      </td>
                    );
                  },
                  status: (item) => (
                    <td>
                      <CBadge color={getBadge(item.status)}>
                        {getStatus(item.status)}
                      </CBadge>
                    </td>
                  ),
                }}
              />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  );
};

export default Suivi;
