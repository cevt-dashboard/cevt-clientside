import React from "react";
import "./calendar.css";
import { CFormGroup, CCol, CButton } from "@coreui/react";
import CIcon from "@coreui/icons-react";

import {
  getTeacherList,
  getGroupDetails,
  getTeacherDetails,
  getAllGroups,
  getTitle,
} from "../../_services/groups.service";
import { addEvent } from "../../_services/events.service";
import {
  Inject,
  ScheduleComponent,
  Day,
  Week,
  WorkWeek,
  Month,
  ViewsDirective,
  ViewDirective,
} from "@syncfusion/ej2-react-schedule";
import {
  Internationalization,
  L10n,
  loadCldr,
  setCulture,
} from "@syncfusion/ej2-base";
import { isNullOrUndefined } from "@syncfusion/ej2-base";
import Select from "react-select";
import { DateTimePickerComponent } from "@syncfusion/ej2-react-calendars";
import { DropDownListComponent } from "@syncfusion/ej2-react-dropdowns";
import { connect } from "react-redux";
import { history } from "../../_helpers/history";

L10n.load({
  "fr-CH": {
    schedule: {
      day: "Jour",
      week: "Semaine",
      workWeek: "Semaine de Travail",
      month: "Mois",
      agenda: "Agenda",
      weekAgenda: "Week Agenda",
      workWeekAgenda: "Work Week Agenda",
      monthAgenda: "Month Agenda",
      today: "Aujourd'hui",
      saveButton: "Sauvegarder",
      cancelButton: "Annuler",
      deleteButton: "Supprimer",
      newEvent: "Demander une résérvation",
    },
  },
});
setCulture("fr-CH");

loadCldr(
  require("cldr-data/supplemental/numberingSystems.json"),
  require("cldr-data/main/fr-CH/ca-gregorian.json"),
  require("cldr-data/main/fr-CH/numbers.json"),
  require("cldr-data/main/fr-CH/timeZoneNames.json")
);

class Calendar extends React.Component {
  constructor(props) {
    super(props);
    this.instance = new Internationalization();
    this.workingDays = [1, 2, 3, 4, 5, 6];

    this.fields = {
      id: "Id",
      subject: { name: "Subject", title: "Intitulé" },
      startTime: { name: "dtStart", title: "Début" },
      endTime: { name: "dtEnd", title: "Fin" },
      location: { name: "Location", title: "Salle" },
      primaryColor: { name: "PrimaryColor" },
    };
    this.state = {
      liste: [],
      cellSpacing: [5, 5],
      activeElement: null,
      displayView: "Week",
      placeholder: "",
      data: [
        {
          Uid: 2,
          Subject: "Meeting",
          dtStart: new Date(2020, 1, 15, 10, 0),
          dtEnd: new Date(2020, 1, 15, 12, 30),
          teacher: "",
          IsAllDay: false,
          Status: "Completed",
          Priority: "High",
          IsReadonly: true,
        },
      ],
      activeSeance: {},
    };
  }

  componentDidMount() {
    window.addEventListener("resize", this.updateDimensions);
    if (this.props.isTeacher) {
      getTeacherList().then((data) => {
        var liste = this.mapTeachers(data);

        if (
          localStorage.getItem("selectedCalendarItemProf") &&
          localStorage.getItem("selectedCalendarItemProf") !== "null"
        ) {
          this.setState({
            activeElement:
              localStorage.getItem("cevt-user").firstName +
              " " +
              localStorage.getItem("cevt-user").lastName,
          });
          this.forceUpdate();
          this.selectTeacher(localStorage.getItem("selectedCalendarItemProf"));
        }
        this.setState({
          liste: liste,
          placeholder: "Selectionner un professeur",
        });
      });
    } else {
      getAllGroups().then((data) => {
        var liste = this.mapGroups(data);
        if (localStorage.getItem("selectedCalendarItemPromo")) {
          this.selectGroup(localStorage.getItem("selectedCalendarItemPromo"));
        }
        this.setState({
          liste: liste,
          placeholder: "Selectionner une promotion",
        });
      });
    }
  }

  // new select box implementation (react-select)
  // react-select requires a list of options passed as an object with two values (value, label)
  mapTeachers = (list) => {
    var result = [];
    list.map((item) => {
      var teacher = {};
      teacher.value = item.firstName + " " + item.lastName;
      teacher.label = item.firstName + " " + item.lastName;
      result.push(teacher);
    });
    return result;
  };

  // new select box implementation (react-select)
  // react-select requires a list of options passed as an object with two values (value, label)
  mapGroups = (list) => {
    var result = [];
    list.map((item) => {
      var group = {};
      group.value = item;
      group.label = item;
      result.push(group);
    });
    return result;
  };

  updateDimensions = () => {
    return window.innerWidth <= 550
      ? this.setState({ displayView: "Day" })
      : this.setState({ displayView: "Week" });
  };

  selectGroup = (event) => {
    var groupName = null;
    if (typeof event == "string") {
      groupName = event;
    } else {
      groupName = event.value;
    }
    getGroupDetails(groupName).then((json) => {
      var datareceived = [];
      json.forEach((value) => {
        var startTime = new Date(value.dtStart);
        startTime.setHours(startTime.getHours() + 1);
        var endTime = new Date(value.dtEnd);
        endTime.setHours(endTime.getHours() + 1);

        datareceived.push({
          Id: value.uid,
          Subject: getTitle(value.matiere),
          dtStart: startTime,
          dtEnd: endTime,
          teacher: value.prof,
          type: value.type,
          IsAllDay: false,
          Location: value.location,
          Priority: "",
          IsReadonly: true,
          SeanceId: value.id,
        });
      });
      localStorage.setItem("selectedCalendarItemPromo", groupName);
      this.setState({
        data: datareceived,
        activeElement: groupName,
      });
    });
  };

  selectTeacher = (event) => {
    var name = null;
    if (typeof event == "string") {
      name = {
        firstName: event.split(" ")[0],
        lastName: event.split(" ")[1],
      };
    } else {
      name = {
        firstName: event.value.split(" ")[0],
        lastName: event.value.split(" ")[1],
      };
    }

    getTeacherDetails(
      name.firstName.toLowerCase(),
      name.lastName.toLowerCase()
    ).then((json) => {
      var datareceived = [];
      json.forEach((value) => {
        var startTime = new Date(value.dtStart);
        startTime.setHours(startTime.getHours() + 1);
        var endTime = new Date(value.dtEnd);
        endTime.setHours(endTime.getHours() + 1);

        datareceived.push({
          Id: value.uid,
          Subject: getTitle(value.matiere),
          dtStart: startTime,
          dtEnd: endTime,
          teacher: value.calName,
          type: value.type,
          IsAllDay: false,
          Location: value.location,
          Priority: "",
          IsReadonly: true,
          Groupe: value.categories,
          SeanceId: value.id,
        });
      });

      this.setState({
        data: datareceived,
        activeElement: name.firstName + " " + name.lastName,
      });

      localStorage.setItem(
        "selectedCalendarItemProf",
        this.state.activeElement
      );
    });
  };
  getDefaultSelectValue() {
    if (this.props.isTeacher) {
      return localStorage.getItem("selectedCalendarItemProf")
        ? localStorage.getItem("selectedCalendarItemProf")
        : "Selectionner un professeur";
    }
    if (!this.props.isTeacher) {
      return localStorage.getItem("selectedCalendarItemPromo")
        ? localStorage.getItem("selectedCalendarItemPromo")
        : "Selectionner une promotion";
    }
  }

  eventTemplateMonth(props) {
    return (
      <div
        className={props.type}
        style={{ width: "212px", textAlign: "center" }}
      >
        <div className="e-subject">{props.Subject}</div>
        <div className="e-location">{props.Location}</div>
      </div>
    );
  }

  splitDate(i) {
    return i < 10 ? (i = "0" + i) : i;
  }

  eventTemplate(props) {
    return (
      <div
        className={props.type}
        style={{
          height: "100%",
          textAlign: "center",
        }}
      >
        <div className="header-Calendar">
          <div className="e-subject">
            {props.type} -{this.splitDate(props.dtStart.getHours())}h
            {this.splitDate(props.dtStart.getMinutes())} -
            {this.splitDate(props.dtEnd.getHours())}h
            {this.splitDate(props.dtEnd.getMinutes())}
          </div>
        </div>
        <div className="body-Calendar">
          <div className="e-title">
            <strong>{props.Subject}</strong>
          </div>
          <div className="e-teacher">{props.teacher}</div>
          {props.Location.trim() !== "" && (
            <div className="e-location">Salle : {props.Location}</div>
          )}
        </div>
      </div>
    );
  }

  onPopupOpen(args) {
    if (args.type === "QuickInfo") args.cancel = true;
    this.setState({ activeSeance: args.data });
    localStorage.setItem("presence-event", JSON.stringify(args.data));
  }

  onPopupClose(args) {
    if (args.type === "Editor" && !isNullOrUndefined(args.data)) {
      var data = {
        // Using timestamps as an id TEMPORARELY !!!
        id: Date.now(),
        prof:
          this.props.user.firstName.toUpperCase() +
          " " +
          this.props.user.lastName.toUpperCase(),
        summary: args.data.summary,
        categories: args.data.groupe,
        dtStart: args.data.StartTime,
        dtEnd: args.data.EndTime,
        location: args.data.salle,
        language: args.data.langue,
        matiere: args.data.matiere,
        type: args.data.categorie,
        groupe: args.data.groupe,
        duree: args.data.EndTime.getTime() - args.data.StartTime.getTime(),
        dtStamp: Date.now(),
        uid: Date.now(),
        custom: "YES",
        valid: "NO",
      };
      addEvent(data);
    }
  }

  handleSubmit() {
    history.push("/#/enseignant/EPresences");
    window.location.reload();
  }

  editorTemplate(props) {
    return props !== undefined ? (
      <table
        className="custom-event-editor"
        style={{ width: "100%", cellpadding: "5" }}
      >
        <tbody>
          <tr>
            <td className="e-textlabel">Intitulé</td>
            <td colSpan={4}>
              <input
                id="Summary"
                className="e-field e-input"
                type="text"
                data-name="summary"
                name="summary"
                style={{ width: "100%" }}
                required={true}
              />
            </td>
          </tr>
          <tr>
            <td className="e-textlabel">Catégorie</td>
            <td colSpan={4}>
              <DropDownListComponent
                id="categorie"
                placeholder="Séléctionner une catégorie"
                data-name="categorie"
                className="e-field"
                style={{ width: "100%" }}
                dataSource={["CM", "TD", "TP", "Autres"]}
                value={props.EventType || null}
              ></DropDownListComponent>
            </td>
          </tr>
          <tr>
            <td className="e-textlabel">Salle</td>
            <td colSpan={4}>
              <input
                id="salle"
                className="e-field e-input"
                type="text"
                data-name="salle"
                name="salle"
                style={{ width: "100%" }}
                required={true}
              />
            </td>
          </tr>
          <tr>
            <td className="e-textlabel">Langue</td>
            <td colSpan={4}>
              <input
                id="langue"
                className="e-field e-input"
                type="text"
                data-name="langue"
                name="langue"
                style={{ width: "100%" }}
                required={true}
              />
            </td>
          </tr>
          <tr>
            <td className="e-textlabel">Matière</td>
            <td colSpan={4}>
              <input
                id="matiere"
                className="e-field e-input"
                type="text"
                data-name="matiere"
                name="matiere"
                style={{ width: "100%" }}
                required={true}
              />
            </td>
          </tr>
          <tr>
            <td className="e-textlabel">Groupe</td>
            <td colSpan={4}>
              <input
                id="groupe"
                className="e-field e-input"
                type="text"
                data-name="groupe"
                name="groupe"
                style={{ width: "100%" }}
                required={true}
              />
            </td>
          </tr>
          <tr>
            <td className="e-textlabel">Début</td>
            <td colSpan={4}>
              <DateTimePickerComponent
                format="dd/MM/yy hh:mm a"
                id="StartTime"
                data-name="StartTime"
                value={new Date(props.startTime || props.StartTime)}
                className="e-field"
              ></DateTimePickerComponent>
            </td>
          </tr>
          <tr>
            <td className="e-textlabel">Fin</td>
            <td colSpan={4}>
              <DateTimePickerComponent
                format="dd/MM/yy hh:mm a"
                id="EndTime"
                data-name="EndTime"
                value={new Date(props.endTime || props.EndTime)}
                className="e-field"
              ></DateTimePickerComponent>
            </td>
          </tr>
        </tbody>
      </table>
    ) : (
      <div></div>
    );
  }

  render() {
    return (
      <>
        <div>
          <CFormGroup row className="centerSelect">
            <CCol xs="10" sm="8" md="6" xl="4">
              {this.props.isTeacher === true ? (
                <Select
                  options={this.state.liste}
                  placeholder={this.getDefaultSelectValue()}
                  onChange={this.selectTeacher}
                />
              ) : (
                <Select
                  options={this.state.liste}
                  placeholder={this.getDefaultSelectValue()}
                  onChange={this.selectGroup}
                />
              )}
            </CCol>
            {this.props.isTeacher === true &&
              Object.keys(this.state.activeSeance) !== 0 && (
                <CCol xs="10" sm="8" md="6" xl="4">
                  <CButton
                    type="submit"
                    onClick={() => this.handleSubmit()}
                    size="sm"
                    color="success"
                    className="mx-1 float-right"
                  >
                    <CIcon name="cil-save" /> Presences
                  </CButton>
                </CCol>
              )}
          </CFormGroup>

          <div>
            {this.state.activeElement !== null && (
              <ScheduleComponent
                cellSpacing={this.state.cellSpacing}
                rowAutoHeight={true}
                height="690px"
                style={{ borderRadius: "1%" }}
                locale="fr-CH"
                currentView={this.state.displayView}
                readonly={!this.props.isTeacher}
                startHour={"08:00"}
                endHour={"20:00"}
                showWeekend={false}
                allowDragAndDrop={false}
                workDays={this.workingDays}
                allowKeyboardInteraction={false}
                selectedDate={new Date()}
                popupOpen={this.onPopupOpen}
                editorTemplate={this.editorTemplate.bind(this)}
                popupClose={this.onPopupClose.bind(this)}
                eventSettings={{
                  dataSource: this.state.data,
                  template: this.eventTemplate.bind(this),
                  fields: this.fields,
                }}
              >
                <Inject services={[Day, Week, WorkWeek, Month]} />
                <ViewsDirective>
                  <ViewDirective option="Day" displayName="Jour" />
                  <ViewDirective option="Week" displayName="Semaine" />
                  {window.innerWidth >= 550 && (
                    <ViewDirective
                      option="Month"
                      displayName="Mois"
                      eventTemplate={this.eventTemplateMonth.bind(this)}
                    />
                  )}
                </ViewsDirective>
              </ScheduleComponent>
            )}
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.authentication.user,
  };
};
export default connect(mapStateToProps)(Calendar);
