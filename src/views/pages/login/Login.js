import React from "react";
import {
  CButton,
  CForm,
  CInput,
  CNavbar,
  CCollapse,
  CCol,
  CLabel,
  CFormText,
  CCardFooter,
  CImg,
  CCard,
  CCardHeader,
  CCardBody,
  CFormGroup,
} from "@coreui/react";
import { connect } from "react-redux";
import { userActions } from "../../../_actions/user.actions";
import Calendar from "../../calendar/Calendar";
import { NavbarBrand } from "react-bootstrap";
import "./Login.scss";
import Slider from "react-animated-slider";
import "react-animated-slider/build/horizontal.css";
import * as Service from "../../../_services/presenceService";
import Moment from "moment";
import * as TMP from "./tmp";
class Login extends React.Component {
  constructor(props) {
    super(props);
    this.props.logout();
    this.state = {
      username: "",
      password: "",
      sidebarOpen: false,
      codePresence: "",
      nomEtu: "",
      prenomEtu: "",
      eventNewsEvolution: {},
    };
  }
  NewlineText(text) {
    return text.split("\n").map((str) => <p>{str}</p>);
  }
  componentDidMount() {
    this.getEventNewsEvolution();
  }
  getEventNewsEvolution() {
    var date = Moment().format("L").split("/");
    Service.getAllNewsEventEvolution(
      date[2] + "-" + date[0] + "-" + date[1]
    ).then((res) => {
      var i;
      let myMap = new Map();
      let tmp = {
        newEvent: [],
        oldEvent: [],
      };

      if (typeof res === "undefined" || res.length <= 0) {
        this.setState({ eventNewsEvolution: TMP.default });
        return;
      }

      res.forEach((x) => {
        tmp.newEvent = [...tmp.newEvent, ...x.newEvent];
        tmp.oldEvent = [...tmp.oldEvent, ...x.oldEvent];
      });

      for (i = 0; i < tmp.newEvent.length; i++) {
        var msg = "** Matiere : " + tmp.newEvent[i].matiere;
        msg =
          msg +
          "   ----   le :  " +
          Moment(new Date(tmp.newEvent[i].dtStart)).format("DD-MM-YYYY HH-mm");
        if (tmp.oldEvent[i].dtStart !== tmp.newEvent[i].dtStart) {
          msg +=
            "\n Nouvelle Date Debut : " +
            Moment(new Date(tmp.newEvent[i].dtStart)).format(
              "DD-MM-YYYY HH-mm"
            );
        }
        if (tmp.oldEvent[i].duree !== tmp.newEvent[i].duree) {
          msg +=
            "\n changement durée : " +
            tmp.newEvent[i].duree +
            " -- Se Termine a :  " +
            Moment(new Date(tmp.newEvent[i].dtEnd)).format("HH-mm");
        }
        if (tmp.oldEvent[i].location !== tmp.newEvent[i].location) {
          msg += "\n changement salle :" + tmp.newEvent[i].location;
        }
        if (myMap.has(tmp.oldEvent[i].categories)) {
          myMap[tmp.oldEvent[i].categories] =
            myMap.get(tmp.oldEvent[i].categories) + "\n" + msg;
        } else {
          myMap.set(tmp.oldEvent[i].categories, msg);
        }
      }
      var arrayResult = [];
      Object.entries(myMap).forEach((x) => {
        arrayResult.push({
          nomFiliere: x[0],
          msg: x[1],
        });
      });

      this.setState({ eventNewsEvolution: arrayResult });

      return arrayResult;
    });
  }
  showSettings(event) {
    event.preventDefault();
  }
  onSetSidebarOpen = (open) => {
    this.setState({ sidebarOpen: open });
  };

  handleChange = (e) => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };

  handleSubmit = (e) => {
    if (this.state.username && this.state.password) {
      const { username, password } = this.state;
      this.props.login(username, password);
    }
  };

  handleChangePresence = (e) => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };

  handleSubmitPresence = (e) => {
    Service.updateTemporaryElement(
      this.state.codePresence,
      `${this.state.nomEtu} _ ${this.state.prenomEtu}`,
      Service.getfingerprint()
    )
      .then((json) => {
        this.setState({
          codePresence: "",
          nomEtu: "",
          prenomEtu: "",
        });
        if (json) {
          window.alert("Enregistrement Reussi ! ");
        } else {
          window.alert("!! ERREUR !! ");
        }
      })
      .catch((err) => {
        window.alert("!! ERREUR !! ");
      });
  };

  shouldComponentUpdate(nextProps, nextState) {
    if (
      this.state.username !== nextState.username ||
      this.state.password !== nextState.password
    ) {
      return false;
    } else {
      return true;
    }
  }

  render() {
    return (
      <div
        id="page-wrap"
        style={{ height: "100%", overflow: "auto" }}
        className="backgroundCustom"
      >
        <a
          style={{ float: "left", height: "10vh" }}
          href="https://drive.google.com/drive/folders/1A3nKa_llxVSPjycskicy2NC6vbnwa2vJ?usp=sharing"
          target="_blank"
        >
          {" "}
          <CImg width="60vh" src={"imgs/play_store.png"}></CImg>{" "}
        </a>
        <CNavbar expandable="sm" style={{ float: "right", height: "10vh" }}>
          <NavbarBrand>
            <a href="https://ecampus.paris-saclay.fr/" target="_blank">
              <CImg width="200vh" src={"imgs/logo2.png"}></CImg>
            </a>
          </NavbarBrand>
        </CNavbar>
        <br></br>

        <Slider
          style={{ height: "84vh" }}
          duration={100}
          animateOut={true}
          minSwipeOffset={"5px"}
        >
          <div className="center">
            <div className="parentDiv">
              <Calendar className="mt-2 childDiv" isTeacher={false} />
            </div>
          </div>
          <div className="center">
            <div className="parentDiv">
              <CCol
                xs="12"
                md="6"
                style={{ margin: "auto", marginTop: "20vh" }}
              >
                <CCard>
                  <CCardHeader>
                    Enseignant
                    <small> authentification </small>
                  </CCardHeader>
                  <CCardBody>
                    <CForm action="" method="post">
                      <CFormGroup>
                        <CLabel htmlFor="nf-email">Nom d'utilisateur</CLabel>
                        <CInput
                          type="text"
                          name="username"
                          autoComplete="username"
                          placeholder="Nom d'utilisateur"
                          onChange={this.handleChange}
                          required
                          className=" "
                        />
                      </CFormGroup>
                      <CFormGroup>
                        <CLabel htmlFor="nf-email">Mot de passe</CLabel>
                        <CInput
                          type="password"
                          name="password"
                          autoComplete="current-password"
                          id="exampleInputEmail2"
                          placeholder="Mot de passe"
                          onChange={this.handleChange}
                          required
                          className=" "
                        />
                        <CFormText
                          className="help-block"
                          style={{ float: "right" }}
                        >
                          Contact:{" "}
                          <a href="mailto:cevt.dll@gmail.com">
                            cevt.dll@gmail.com
                          </a>
                        </CFormText>
                      </CFormGroup>
                    </CForm>
                  </CCardBody>
                  <CCardFooter>
                    <CButton
                      size="sm"
                      color="primary"
                      onClick={this.handleSubmit}
                    >
                      {" "}
                      Valider
                    </CButton>
                  </CCardFooter>
                </CCard>
              </CCol>
            </div>
          </div>

          <div className="center">
            <div
              className="parentDiv"
              style={{
                overflow: "scroll",
              }}
            >
              <CCard
                className="card mb-3"
                style={{
                  margin: "auto",
                  marginTop: "",
                  overflow: "scroll",
                }}
              >
                <CCardHeader>
                  Actualité pour Classes
                  <small> IBGBI</small>
                </CCardHeader>
                <CCardBody>
                  {Array.isArray(this.state.eventNewsEvolution) &&
                    this.state.eventNewsEvolution.map((user, index) => (
                      <div id="accordion">
                        <CCard className="mb-0 card   mb-3">
                          <CCardHeader id="headingOne">
                            <CButton
                              block
                              color="link"
                              className="text-left m-0 p-0"
                            >
                              <h5 className="m-0 p-0"> {user.nomFiliere}</h5>
                            </CButton>
                          </CCardHeader>
                          <CCollapse show={true}>
                            <CCardBody>{this.NewlineText(user.msg)}</CCardBody>
                          </CCollapse>
                        </CCard>
                      </div>
                    ))}
                </CCardBody>
              </CCard>
            </div>
          </div>

          <div className="center">
            <div className="parentDiv">
              <CCol
                xs="12"
                md="6"
                style={{ margin: "auto", marginTop: "20vh" }}
              >
                <CCard>
                  <CCardHeader>
                    Présence
                    <small> Etudiant</small>
                  </CCardHeader>
                  <CCardBody>
                    <CForm action="" method="post">
                      <CFormGroup>
                        <CInput
                          type="text"
                          name="codePresence"
                          autoComplete="codePresence"
                          placeholder="Code"
                          onChange={this.handleChangePresence}
                          required
                          className=" "
                        />
                      </CFormGroup>

                      <CFormGroup>
                        <CInput
                          type="text"
                          name="nomEtu"
                          autoComplete="nomEtu"
                          placeholder="Nom"
                          onChange={this.handleChangePresence}
                          required
                          className=" "
                        />
                        <CFormText className="help-block"></CFormText>
                      </CFormGroup>
                      <CFormGroup>
                        <CInput
                          type="text"
                          name="prenomEtu"
                          autoComplete="prenomEtu"
                          placeholder="Prenom"
                          onChange={this.handleChangePresence}
                          required
                          className=" "
                        />
                      </CFormGroup>
                    </CForm>
                  </CCardBody>
                  <CCardFooter>
                    <CButton
                      size="sm"
                      color="primary"
                      onClick={this.handleSubmitPresence}
                    >
                      {" "}
                      Valider
                    </CButton>
                  </CCardFooter>
                </CCard>
              </CCol>
            </div>
          </div>
        </Slider>

        <div
          className="d-flex justify-content-center white"
          style={{ height: "6vh" }}
        >
          <p style={{ color: "white", textAlign: "center" }}>
            Conçu avec L'equipe ASYB, Etudiants Miage 2 Alternance. Copyright ©{" "}
            {new Date().getFullYear()} <em>Cc BY-NC-SA 4.0 </em>
          </p>
        </div>
      </div>
    );
  }
}

function mapState(state) {
  const { loggingIn } = state.authentication;
  return { loggingIn };
}

const actionCreators = {
  login: userActions.login,
  logout: userActions.logout,
};

export default connect(mapState, actionCreators)(Login);
