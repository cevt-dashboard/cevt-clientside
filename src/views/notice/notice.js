import React from "react";
import { CCard, CCardHeader, CCardBody } from "@coreui/react";

const notice = () => {
  return (
    <>
      <CCard>
        <CCardHeader>
          <h3 className="text-center">Mentions légales</h3>
        </CCardHeader>
        <CCardBody className="text-justify text-center">
          <p>
            Merci de lire attentivement les présentes modalités d'utilisation du
            présent site avant de le parcourir. En vous connectant sur ce site,
            vous acceptez sans réserve les présentes modalités.
          </p>
          <p>
            <strong>Editeur du site</strong>
            <br />
            Site Internet CEVT
            <br />
            Par l'équipe CEVT de l'université d’evry val d’essonne
            <br />
            23 Boulevard François Mitterrand, <br />
            91000 Évry-Courcouronnes
            <br />
            France
            <br />
            Tél. : + 33 (0)1 69 47 70 00
            <br />
            <a href="https://gitlab.com/cevt-dashboard">
              https://gitlab.com/cevt-dashboard
            </a>
          </p>
          <p>
            <strong>Conditions d'utilisation</strong> <br />
            Tout le contenu du présent site, incluant, de façon non limitative,
            les graphismes, images, textes, vidéos, animations, sons, logos,
            gifs et icônes ainsi que leur mise en forme sont la propriété
            exclusive de l'équipe CEVT. Toute reproduction, distribution,
            modification, adaptation, retransmission ou publication, même
            partielle, de ces différents éléments est strictement interdite sans
            l'accord exprès par écrit de l'équipe CEVT. Cette représentation ou
            reproduction, par quelque procédé que ce soit, constitue une
            contrefaçon sanctionnée par les articles L.335-2 et suivants du Code
            de la propriété intellectuelle. Le non-respect de cette interdiction
            constitue une contrefaçon pouvant engager la responsabilité civile
            et pénale du contrefacteur. En outre, les propriétaires des Contenus
            copiés pourraient intenter une action en justice à votre encontre.
          </p>
          <p>
            En utilisant le site, vous reconnaissez avoir pris connaissance de
            ces conditions et les avoir acceptées. Celles-ci pourront être
            modifiées à tout moment et sans préavis par l'équipe CEVT. L'équipe
            CEVT ne saurait être tenue pour responsable en aucune manière d’une
            mauvaise utilisation du service.
          </p>
          <p>
            <strong>Hébergeur</strong>
            <br />
            OVHcloud
            <br />
            Plateforme qui a pour cœur de métier l’hébergement de serveurs
            <br />
            <a href="https://www.ovh.com/fr/">https://www.ovh.com/fr/</a>
          </p>
          <p>
            <strong>Données personnelles</strong>
            <br />
            D'une façon générale, vous pouvez visiter notre site sur Internet
            sans avoir à décliner votre identité et à fournir des informations
            personnelles vous concernant. Cependant, ce principe comporte
            certaines exceptions. En effet, si vous êtes professeur, vous pouvez
            être amenés à nous communiquer certaines données afin d'accéder à
            votre espace personnel. Dans tous les cas vos données personnelles
            sont confidentielles et ne seront en aucun cas communiquées à des
            tiers hormis pour la bonne exécution de la prestation.
          </p>
          <p>
            <strong>Contactez-nous</strong>
            <br />
            L'équipe CEVT est à votre disposition pour tous vos commentaires ou
            suggestions. Vous pouvez nous écrire en français par courrier
            électronique à :{" "}
            <a href="mailto:cevt.dll@gmail.com">cevt.dll@gmail.com</a>
          </p>
          <p>
            <strong>Licence</strong>
            <br />
            <em>CC-by-nc-sa-4.0 </em>
          </p>
        </CCardBody>
      </CCard>
    </>
  );
};

export default notice;
