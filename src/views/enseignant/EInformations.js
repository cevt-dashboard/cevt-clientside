import React, { Component } from "react";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CButton,
} from "@coreui/react";
import Moment from "moment";
import * as Service from "../../_services/teachers.service";

const getBadge = (status) => {
  switch (status) {
    case "Validé":
      return "success";
    case "Error":
      return "secondary";
    case "En Attente":
      return "warning";
    case "Refusé":
      return "danger";
    default:
      return "secondary";
  }
};

class EInformations extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allMatiere: [],
      allMatiereStats: {},
      statsTeacherDetails: {},
      myCustomEventsList: [],
    };
  }

  componentDidMount() {
    this.initData();
    this.getCustomSeance();
  }
  componentWillUnmount() {}

  initData() {
    const promises = [];
    const user = JSON.parse(localStorage.getItem("cevt-user"));
    promises.push(
      Service.getTeachersMatiereList(user.firstName, user.lastName)
    );
    promises.push(
      Service.getTeachersMatiereStats(user.firstName, user.lastName)
    );
    promises.push(
      Service.getStatsTeacherDetails(user.firstName, user.lastName)
    );

    Promise.all(promises).then((result) => {
      this.setState({
        allMatiere: result[0],
        allMatiereStats: this.adaptFirstTable(result[1]),
        statsTeacherDetails: this.getEvolutionglobal(result[2]),
      });
    });
  }

  getCustomSeance() {
    const user = JSON.parse(localStorage.getItem("cevt-user"));
    Service.getMyCustomEvents(user.username)
      .then((json) => json.json())
      .then((data) => {
        data.forEach((x) => {
          if (x.valid === "NO") {
            x.status = "En Attente";
          }
          if (x.valid === "REFUSED") {
            x.status = "Refusé";
          }
          if (x.valid === "YES") {
            x.status = "Validé";
          }
        });

        this.setState({
          myCustomEventsList: data,
        });
      });
  }
  getStatsCompressed(item) {
    return {
      done: item.cmDone + item.tdDone + item.tpDone,
      total: item.cmTotal + item.tdTotal + item.tpTotal,
    };
  }
  getEvolutionglobal(item) {
    var tab = [];

    tab.push(
      ...[
        {
          type: "CM",
          done: item.Done.cm,
          total: item.Total.cm,
          status: item.Done.cm === item.Total.cm ? "Validé" : "En Attente",
        },
        {
          type: "TD",
          done: item.Done.td,
          total: item.Total.td,
          status: item.Done.td === item.Total.td ? "Validé" : "En Attente",
        },
        {
          type: "TP",
          done: item.Done.tp,
          total: item.Total.tp,
          status: item.Done.tp === item.Total.tp ? "Validé" : "En Attente",
        },
      ]
    );

    return tab;
  }
  adaptFirstTable(items) {
    var arrayResult = [];
    Object.entries(items).forEach((x) => {
      var tmp = this.getStatsCompressed(x[1]);
      arrayResult.push({
        name: x[0],
        date_debut: new Intl.DateTimeFormat().format(new Date(x[1].date_debut)),
        date_fin: new Intl.DateTimeFormat().format(new Date(x[1].date_fin)),
        volume_horaire: tmp.total,
        percent_evolution: ((tmp.done / tmp.total) * 100).toFixed(2) + "%",
        status: tmp.done === tmp.total ? "Validé" : "En Attente",
        brut_values: x[1],
      });
    });

    return arrayResult;
  }

  deleteCustomSeance(item) {
    Service.deleteCustomSeanceTeacher(item.id).then((response) => {
      window.alert("Deleted successfully");
      this.getCustomSeance();
      this.forceUpdate();
    });
  }

  render() {
    return (
      <>
        <CRow>
          {Object.keys(this.state.allMatiereStats).length !== 0 && (
            <CCol xs="5" lg="12">
              <CCard>
                <CCardHeader>
                  <strong> Enseignement Assignés</strong>
                </CCardHeader>
                <CCardBody>
                  <CDataTable
                    items={this.state.allMatiereStats}
                    fields={[
                      {
                        key: "name",
                        label: "Matiere",
                        _classes: "font-weight-bold",
                      },
                      {
                        key: "date_debut",
                        label: "Date Debut",
                      },
                      {
                        key: "date_fin",
                        label: "Date Fin",
                      },
                      {
                        key: "volume_horaire",
                        label: "Volume Horaire",
                        filter: false,
                      },
                      {
                        key: "percent_evolution",
                        label: "Evolution",
                        sorter: false,
                        filter: false,
                      },
                      {
                        key: "status",
                        label: "Status",
                      },
                    ]}
                    columnFilter
                    tableFilter
                    itemsPerPageSelect
                    itemsPerPage={5}
                    hover
                    sorter
                    pagination
                    striped={true}
                    responsive={true}
                    scopedSlots={{
                      status: (item) => (
                        <td>
                          <CBadge color={getBadge(item.status)}>
                            {item.status}
                          </CBadge>
                        </td>
                      ),
                    }}
                  />
                </CCardBody>
              </CCard>
            </CCol>
          )}
          {Object.keys(this.state.statsTeacherDetails).length !== 0 && (
            <CCol xs="12" lg="12">
              <CCard>
                <CCardHeader>
                  <strong> Evolution horaire Globale </strong>
                </CCardHeader>
                <CCardBody>
                  <CDataTable
                    items={this.state.statsTeacherDetails}
                    fields={[
                      { key: "type", _classes: "font-weight-bold" },
                      "total",
                      "done",
                      "status",
                    ]}
                    columnFilter
                    tableFilter
                    itemsPerPageSelect
                    itemsPerPage={5}
                    hover
                    sorter
                    pagination
                    striped={true}
                    responsive={true}
                    scopedSlots={{
                      status: (item) => (
                        <td>
                          <CBadge color={getBadge(item.status)}>
                            {item.status}
                          </CBadge>
                        </td>
                      ),
                    }}
                  />
                </CCardBody>
              </CCard>
            </CCol>
          )}
        </CRow>
        <CRow>
          {Object.keys(this.state.statsTeacherDetails).length !== 0 && (
            <CCol xs="12" lg="12">
              <CCard>
                <CCardHeader>
                  <strong>Events en attentes de validation </strong>
                </CCardHeader>
                <CCardBody>
                  <CDataTable
                    items={this.state.myCustomEventsList}
                    fields={[
                      {
                        key: "groupe",
                        label: "filiere",
                        _classes: "font-weight-bold",
                      },
                      { key: "dtStart", label: "Date" },
                      { key: "duree", label: "Durée" },
                      { key: "summary", label: "Titre" },
                      { key: "type", label: "Type" },
                      { key: "status", label: "Status" },
                      {
                        key: "actions",
                        label: "Delete",
                        sorter: false,
                        filter: false,
                      },
                    ]}
                    columnFilter
                    tableFilter
                    itemsPerPageSelect
                    itemsPerPage={5}
                    hover
                    sorter
                    pagination
                    striped={true}
                    responsive={true}
                    scopedSlots={{
                      status: (item) => (
                        <td>
                          <CBadge color={getBadge(item.status)}>
                            {item.status}
                          </CBadge>
                        </td>
                      ),
                      dtStart: (item) => (
                        <td>
                          {Moment(item.dtStart).format("DD MM YYYY hh:mma")}
                        </td>
                      ),
                      duree: (item) => (
                        <td>
                          {Moment.duration(item.duree).hours() +
                            "h" +
                            Moment.duration(item.duree).minutes() +
                            "m"}
                        </td>
                      ),
                      actions: (item) => {
                        return (
                          <td>
                            <CButton
                              variant="outline"
                              color="danger"
                              onClick={() => {
                                this.deleteCustomSeance(item);
                              }}
                            >
                              X
                            </CButton>
                          </td>
                        );
                      },
                    }}
                  />
                </CCardBody>
              </CCard>
            </CCol>
          )}
        </CRow>
      </>
    );
  }
}
export default EInformations;
