import React, { Component } from "react";
import * as Service from "../../_services/presenceService";
import "jspdf-autotable";

import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CInput,
  CButton,
  CFormGroup,
  CCardFooter,
  CTabPane,
  CTabs,
  CNav,
  CNavItem,
  CNavLink,
  CTabContent,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { jsPDF } from "jspdf";
import Moment from "moment";

const getBadge = (status) => {
  switch (status) {
    case "Active":
      return "success";
    case "Inactive":
      return "secondary";
    case "Pending":
      return "warning";
    case "Banned":
      return "danger";
    default:
      return "primary";
  }
};
class EPresences extends Component {
  constructor(props) {
    super(props);
    this.showNumber = false;

    this.state = {
      tmpObject: {},
      tmpEtudiant_nom: "",
      tmpEtudiant_prenom: "",
      sortedData: [],
      allPresence: [],
      uniqueFingerprint: "",
      tmpSeance: JSON.parse(localStorage.getItem("presence-event")),
      intervalId: setInterval(this.timer, 2000),
    };
    this.addEtudiant = this.addEtudiant.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    clearInterval(this.state.intervalId);
    this.loadAllMyPresence(
      JSON.parse(localStorage.getItem("cevt-user")).username
    );
  }
  componentWillUnmount() {
    clearInterval(this.state.intervalId);
  }
  formatAffichage(num) {
    return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
  }

  addEtudiant = (e) => {
    if (e.target.id === "nom") {
      this.setState({ tmpEtudiant_nom: e.target.value });
    }
    if (e.target.id === "prenom") {
      this.setState({ tmpEtudiant_prenom: e.target.value });
    }
  };
  handleSubmit = (e) => {
    Service.updateTemporaryElement(
      this.state.tmpObject.code,
      `${this.state.tmpEtudiant_prenom} _ ${this.state.tmpEtudiant_nom}`,
      "specialauthorisationprof"
    ).then((json) => {
      this.updateListPresence();
    });

    e.preventDefault();
  };

  timer = () => {
    this.updateListPresence();
  };
  loadAllMyPresence(enseignantName) {
    Service.getAllElementForEnseignant(enseignantName).then((json) => {
      this.setState({ allPresence: json });
    });
  }
  updateListPresence() {
    if (!this.state && this.hideBody()) {
      return;
    } else {
      Service.findTemporaryElement(this.state.tmpObject.code).then((json) => {
        //this.state.allPresence.push(this.state.tmpObject);
        this.setState({ tmpObject: json });
        this.constuctDataTableData(this.state.tmpObject.etudiants);
      });
    }
  }

  constuctDataTableData(data) {
    var tt = [];
    var i = 1;

    if (data) {
      Object.entries(data).forEach((x) => {
        tt.push({
          name: x[0],
          classement: i,
        });
        i++;
      });
    }

    this.setState({ sortedData: tt });
    this.forceUpdate();
  }

  getTmpObject() {
    //TODO GET FROM OTHER COMPOEMENT
    const tmpSeanceID = JSON.parse(localStorage.getItem("presence-event"))
      .SeanceId;
    const filiere = JSON.parse(localStorage.getItem("presence-event")).Groupe;
    const tmpEnseignant = JSON.parse(localStorage.getItem("cevt-user"))
      .username;
    Service.getTemporaryElement(tmpEnseignant, tmpSeanceID, filiere).then(
      (json) => {
        this.showNumber = true;
        this.setState({
          tmpObject: json,
          intervalId: setInterval(this.timer, 2000),
        });
      }
    );
  }
  generatePDF(item) {
    const user = JSON.parse(localStorage.getItem("cevt-user"));
    Service.getMyCustomEvents(user.firstName + " - " + user.lastName);
    var doc = new jsPDF();
    var img = new Image();
    img.src = "imgs/ueve_logo.png";
    doc.addImage(img, "png", 10, 0, 60, 25);

    var y = 30;
    doc.autoTable({
      margin: { top: y },
      columns: [
        { header: "Université", dataKey: "univ" },
        { header: "Filiere", dataKey: "filiere" },
        { header: "Enseignant", dataKey: "prof" },
        { header: "Localisation", dataKey: "localisation" },
        { header: "Matiere", dataKey: "matiere" },
      ],
      body: [
        {
          univ: "Evry",
          filiere: item.filiere,
          prof: `${user.firstName} ${user.lastName}`,
          localisation: JSON.parse(localStorage.getItem("presence-event"))
            .Location,
          matiere: JSON.parse(localStorage.getItem("presence-event")).Subject,
        },
      ],
    });

    // Example usage of columns property. Note that America will not be included even though it exist in the body since there is no column specified for it.
    doc.autoTable({
      margin: { top: y },
      columns: [
        { header: "N", dataKey: "numero" },
        { header: "Nom", dataKey: "nom" },
        { header: "Prenom", dataKey: "prenom" },
      ],
      body: Object.entries(item.etudiants).map((etu, index) => {
        return {
          numero: index + 1,
          prenom: etu[0].split("_")[1],
          nom: etu[0].split("_")[0],
        };
      }),
    });

    doc.setFontSize(9);
    doc.text(Moment(new Date()).format("DD-MM-YYYY hh:mm"), 160, 15);

    doc.save(item.filiere + "_" + item.enseignant + ".pdf");
  }

  deletePresence(id) {
    Service.deletePresence(id).then((json) => {
      this.setState({
        allPresence: this.state.allPresence.filter(function (person) {
          return person.id !== id;
        }),
      });
    });
  }

  toggleDetails = (index) => {
    const position = this.state.allPresence.indexOf(index);
    let newDetails = this.state.allPresence.slice();
    if (position !== -1) {
      newDetails.splice(position, 1);
    } else {
      newDetails = [...this.state.allPresence, index];
    }
    this.setState({ allPresence: newDetails });
  };
  close() {
    Service.closeAppel(this.state.tmpObject.code).then((json) => {
      this.updateListPresence();

      this.setState({
        tmpObject: {},
      });
      window.location.reload();
    });
  }
  render() {
    return (
      <>
        {this.state.tmpSeance && (
          <CCol xs="10" lg="10">
            <CCard>
              <CCardBody>
                {" "}
                <table className="table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th>Matiere</th>
                      <th>Filiere</th>
                      <th>Debut</th>
                      <th>Fin</th>
                      <th>Salle</th>
                      <th>Type</th>
                    </tr>
                  </thead>

                  <tbody>
                    <tr>
                      <th scope="row">{this.state.tmpSeance.Subject}</th>
                      <th scope="row">{this.state.tmpSeance.Groupe}</th>
                      <td>
                        {Moment(this.state.tmpSeance.dtStart).format(
                          "DD-MM-YYYY hh:mma"
                        )}
                      </td>
                      <td>
                        {Moment(this.state.tmpSeance.dtEnd).format(
                          "DD-MM-YYYY hh:mma"
                        )}
                      </td>
                      <td>{this.state.tmpSeance.Location}</td>
                      <td>{this.state.tmpSeance.type}</td>
                    </tr>
                  </tbody>
                </table>
              </CCardBody>
            </CCard>

            <CCard color="gradient-secondary">
              <CCardHeader>
                <div className="container">
                  <div className="row">
                    <div className="col-sm">
                      <CButton
                        variant="outline"
                        color="primary"
                        onClick={() => this.getTmpObject()}
                        size="lg"
                      >
                        Débuter l'appel
                      </CButton>
                    </div>
                    <div className="col-sm">
                      <CButton
                        type="reset"
                        variant="outline"
                        className="float-right mr-3"
                        size="lg"
                        color="danger"
                        onClick={this.hideBody}
                      >
                        <CIcon name="cil-ban" /> Arreter
                      </CButton>
                    </div>
                  </div>
                </div>
              </CCardHeader>

              {this.showNumber === true && (
                <CCardBody>
                  <p
                    style={{
                      fontSize: "72px",
                      margin: "0 auto",
                      textAlign: "center",
                    }}
                  >
                    {this.formatAffichage(this.state.tmpObject.code)}{" "}
                  </p>
                </CCardBody>
              )}
            </CCard>
          </CCol>
        )}
        <CCol xs="10" lg="10">
          <CCard>
            <CCardBody>
              <CTabs activeTab="Avancement">
                <CNav variant="tabs">
                  <CNavItem>
                    <CNavLink data-tab="Avancement">Avancement</CNavLink>
                  </CNavItem>
                  <CNavItem>
                    <CNavLink data-tab="Historique">Historique</CNavLink>
                  </CNavItem>
                </CNav>
                <CTabContent>
                  <CTabPane data-tab="Avancement">
                    <br></br>

                    <CCard>
                      <CCardHeader>
                        Ceux qui ont validé sont ...
                        <small className="text-muted"> ladder</small>
                      </CCardHeader>
                      {this.showNumber === true && (
                        <CCardBody>
                          <CDataTable
                            items={this.state.sortedData}
                            fields={[
                              { key: "name", _classes: "font-weight-bold" },
                              "classement",
                            ]}
                            hover
                            striped
                            itemsPerPage={5}
                            clickableRows
                            scopedSlots={{
                              status: (item) => (
                                <td>
                                  <CBadge color={getBadge(item.status)}>
                                    {item.status}
                                  </CBadge>
                                </td>
                              ),
                            }}
                          />
                        </CCardBody>
                      )}

                      <CCardFooter>
                        <CFormGroup row className="my-0">
                          <CCol xs="4">
                            <CFormGroup>
                              <CInput
                                id="nom"
                                size="sm"
                                type="text"
                                onChange={this.addEtudiant}
                                placeholder="Nom"
                                required
                              />
                            </CFormGroup>
                            <CFormGroup>
                              <CInput
                                id="prenom"
                                size="sm"
                                type="text"
                                onChange={this.addEtudiant}
                                placeholder="Prenom"
                                required
                              />
                            </CFormGroup>
                          </CCol>
                        </CFormGroup>
                        <CCol xs="4">
                          <CFormGroup>
                            <CButton
                              type="submit"
                              size="sm"
                              color="primary"
                              onClick={this.handleSubmit}
                            >
                              Insérer
                            </CButton>
                          </CFormGroup>
                        </CCol>
                      </CCardFooter>
                    </CCard>
                  </CTabPane>
                  <CTabPane data-tab="Historique">
                    <CCard>
                      <CCardHeader>
                        Anciennes seances validées ...
                        <small className="text-muted"> </small>
                      </CCardHeader>
                      {this.state.allPresence && (
                        <CCardBody>
                          <CDataTable
                            items={this.state.allPresence}
                            fields={[
                              { key: "filiere", _classes: "font-weight-bold" },
                              { key: "etudiants", label: "Nb Etudiants" },
                              { key: "creation_Date", label: "Date" },
                              { key: "pdf", label: "Download" },
                              { key: "delete", label: "Delete" },
                            ]}
                            hover
                            striped
                            itemsPerPage={5}
                            clickableRows
                            pagination={true}
                            scopedSlots={{
                              creation_Date: (item) => (
                                <td>
                                  {Moment(item.creation_Date).format(
                                    "DD-MM-YYYY hh:mma"
                                  )}
                                </td>
                              ),
                              etudiants: (item) => (
                                <td>
                                  <CBadge>
                                    {Object.entries(item.etudiants).length}
                                  </CBadge>
                                </td>
                              ),
                              pdf: (item) => {
                                return (
                                  <td>
                                    <CButton
                                      variant="outline"
                                      color="primary"
                                      onClick={() => {
                                        this.generatePDF(item);
                                      }}
                                    >
                                      <CIcon
                                        name="cil-cloud-download"
                                        height="20"
                                        alt="Logo"
                                      />
                                    </CButton>
                                  </td>
                                );
                              },
                              delete: (item) => {
                                return (
                                  <td>
                                    <CButton
                                      variant="outline"
                                      color="danger"
                                      onClick={() => {
                                        this.deletePresence(item.id);
                                      }}
                                    >
                                      X
                                    </CButton>
                                  </td>
                                );
                              },
                            }}
                          />
                        </CCardBody>
                      )}
                    </CCard>
                  </CTabPane>
                </CTabContent>
              </CTabs>
            </CCardBody>
          </CCard>
        </CCol>
      </>
    );
  }

  hideBody = () => {
    this.showNumber = false;
    this.close();
    clearInterval(this.state.intervalId);
    this.forceUpdate();
  };
}

export default EPresences;
