import { combineReducers } from 'redux';
import {authentication} from './authentication.reducer'

const initialState = {
  sidebarShow: 'responsive'
}

const eventPresence = {}

const sideBar = (state = initialState, { type, ...rest }) => {
  switch (type) {
    case 'set':
      return {...state, ...rest }
    default:
      return state
  }
}

const presence = (state = eventPresence, { type, ...rest }) => {
  switch (type) {
    case 'set':
      return {...state, ...rest }
    default:
      return state
  }
}

const rootReducer = combineReducers({
    authentication,
    sideBar,
    presence
  });

export default rootReducer