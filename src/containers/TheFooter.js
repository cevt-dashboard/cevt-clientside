import React from "react";
import { CFooter } from "@coreui/react";

const TheFooter = () => {
  return (
    <CFooter fixed={false}>
      <div>
        <span className="ml-1">
          CEVT &copy; {new Date().getFullYear()} Univ Evry
        </span>
      </div>

      <div className="mfs-auto">
        <span className="mr-1">
          Last Data update : {new Intl.DateTimeFormat().format(new Date())}
        </span>
      </div>
    </CFooter>
  );
};

export default React.memo(TheFooter);
