export default [
  {
    _tag: "CSidebarNavItem",
    name: "Dashboard",
    to: "/dashboard",
    icon: "cil-speedometer",
    badge: {
      color: "info",
    },
  },
  {
    _tag: "CSidebarNavItem",
    name: "Statistiques",
    to: "/charts",
    icon: "cil-chart-pie",
  },

  {
    _tag: "CSidebarNavTitle",
    _children: ["Outils"],
  },
  {
    _tag: "CSidebarNavItem",
    name: "Calendrier",
    to: "/calendarEnseignant",
    icon: "cil-calendar",
  },
  {
    _tag: "CSidebarNavDropdown",
    name: "Enseignant",
    icon: "cil-people",
    _children: [
      {
        _tag: "CSidebarNavItem",
        name: "Informations",
        to: "/enseignant/EInformations",
      },
      {
        _tag: "CSidebarNavItem",
        name: "Presences",
        to: "/enseignant/EPresences",
      },
    ],
  },
  {
    _tag: "CSidebarNavDropdown",
    name: "Administrateur",
    route: "/buttons",
    icon: "cil-clipboard",
    _children: [
      {
        _tag: "CSidebarNavItem",
        name: "Suivi",
        to: "/admin/gestion/suivi",
      },
      {
        _tag: "CSidebarNavItem",
        name: "Gestion enseignants",
        to: "/admin/gestion/enseignants",
      },
    ],
  },
  {
    _tag: "CSidebarNavItem",
    name: "Informations",
    route: "/informations",
    to: "/informations",
    icon: "cil-bell",
  },

  {
    _tag: "CSidebarNavDivider",
  },
  {
    _tag: "CSidebarNavTitle",
    _children: ["Extras"],
  },
  {
    _tag: "CSidebarNavItem",
    name: "Profile",
    to: "/profile",
    icon: "cil-user",
  },
  {
    _tag: "CSidebarNavItem",
    name: "Mentions légales",
    to: "/notice",
    icon: "cil-notes",
  },
  {
    _tag: "CSidebarNavDivider",
    className: "m-2",
  },
];
