import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  CCreateElement,
  CSidebar,
  CSidebarBrand,
  CSidebarNav,
  CSidebarNavDivider,
  CSidebarNavTitle,
  CSidebarMinimizer,
  CSidebarNavDropdown,
  CSidebarNavItem,
  CImg,
} from "@coreui/react";
import { store } from "../store";

// sidebar nav config
import navigation from "./_nav";

const TheSidebar = () => {
  const dispatch = useDispatch();
  const user = store.getState().authentication.user;
  const show = useSelector((state) => state.sideBar.sidebarShow);
  const [filteredNavigation, filterNavigation] = useState(navigation);

  useEffect(() => {
    if (!user.roles.includes("ROLE_ADMIN")) {
      filterNavigation(
        navigation.filter((element) => element.name !== "Administrateur")
      );
    }
  }, []);

  return (
    <CSidebar
      show={show}
      onShowChange={(val) => dispatch({ type: "set", sidebarShow: val })}
    >
      <CSidebarBrand className="d-md-down-none" to="/">
        <CImg width="150" height="55" src={"imgs/logo2.png"}></CImg>
      </CSidebarBrand>
      <CSidebarNav>
        <CCreateElement
          items={filteredNavigation}
          components={{
            CSidebarNavDivider,
            CSidebarNavDropdown,
            CSidebarNavItem,
            CSidebarNavTitle,
          }}
        />
      </CSidebarNav>
      <CSidebarMinimizer className="c-d-md-down-none" />
    </CSidebar>
  );
};

export default React.memo(TheSidebar);
