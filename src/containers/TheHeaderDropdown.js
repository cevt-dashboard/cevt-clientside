import React from "react";
import {
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CImg
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { withRouter } from 'react-router-dom';
import { connect } from "react-redux";

class TheHeaderDropdown extends React.Component {
  render() {
  return (
    <CDropdown
      inNav
      className="c-header-nav-items mx-2"
      direction="down"
    >
      <CDropdownToggle className="c-header-nav-link" caret={false}>
        <div className="c-avatar">
          <CImg
            src={'avatars/profile.png'}
            className="c-avatar-img"
            alt="Profile picture"
          />
        </div>
      </CDropdownToggle>
      <CDropdownMenu className="pt-0" placement="bottom-end">
        <CDropdownItem
          header
          tag="div"
          color="light"
          className="text-center"
        >
          <strong>{this.props.user.firstName.charAt(0).toUpperCase() + this.props.user.firstName.slice(1) +" " +this.props.user.lastName.toUpperCase()}</strong>
        </CDropdownItem>
        <CDropdownItem onClick={e => this.props.history.push('/profile')}>
          <CIcon name="cil-user" className="mfe-2" /> 
          Profile
        </CDropdownItem>
        <CDropdownItem divider />
        <CDropdownItem onClick={e => this.props.history.push('/login')}>
          <CIcon name="cil-lock-locked" className="mfe-2" /> 
          Se déconnecter
        </CDropdownItem>
      </CDropdownMenu>
    </CDropdown>
  )
}
}

const mapStateToProps = (state) => {
  return {
    user: state.authentication.user,
  };
};
export default connect(mapStateToProps)(withRouter(TheHeaderDropdown))
