export function addEvent(data) {
  const requestOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization:
        "Bearer " + JSON.parse(localStorage.getItem("cevt-user")).token,
    },
    body: JSON.stringify({ ...data, ...{creator: JSON.parse(localStorage.getItem("cevt-user")).username}}),
  };
  var response = fetch(
    process.env.REACT_APP_API_URL + `/api/v1/events/add`,
    requestOptions
  ).then((response) => response.json());
  window.alert('Votre demande a été envoyée avec succès !\n Un administrateur va consulter votre demande afin d\'apporter une décision.')

  return response;
}

 