export const CM = "CM";
export const TP = "TP";
export const TD = "TD";



export const DashboardFields = [
    { key: 'calName', label: 'Enseignant',  _style: { width: '40%'} },
    { key: 'cm', label: 'CM Effectué',  _style: { width: '20%'} },
    { key: 'td', label: 'TD Effectué',  _style: { width: '20%'} },
    { key: 'tp',  label: 'TP Effectué', _style: { width: '20%'} }
  ];

  export const AvancementMatiereEnseignantFields = [
    { key: 'matiereName', label: 'Name',  _style: { width: '15%'} },
    { key: 'cmTotal', label: 'Total CM',  _style: { width: '15%'} },
    { key: 'cmDone', label: 'CM Effectué',  _style: { width: '15%'} },
    { key: 'tdTotal', label: 'Total TD',  _style: { width: '15%'} },
    { key: 'tdDone', label: 'Td Effectué',  _style: { width: '15%'} },
    { key: 'tpTotal', label: 'Total TP',  _style: { width: '15%'} },
    { key: 'tpDone', label: 'TP Effectué',  _style: { width: '15%'} },
  ];  

  export const GroupFields = [
    { key: 'name', label: 'Nom du groupe',  _style: { width: '20%'} },
    { key: 'link', label: 'URL du calendrier ICS',  _style: { width: '80%'} },
    {
      key: 'actions',
      label: 'Actions',
      _style: { width: '1%' },
      sorter: false,
      filter: false
    }
  ];

  export const TeachersFields = [
    { key: 'lastName', label: 'Nom' },
    { key: 'firstName', label: 'Prénom' },
    { key: 'email', label: 'Email' },
    { key: 'status', label: 'Status' },
    {
      key: 'actions',
      label: 'Actions',
      _style: { width: '1%' },
      sorter: false,
      filter: false
    }
  ];

  export const AdminFields = [
    {
      key: "name",
      label: "Matiere",
      _classes: "font-weight-bold",
    },
    {
      key: "prof",
      label: "Prof",
      _classes: "font-weight-bold",
    },
    {
      key: "date_debut",
      label: "Date Debut",
    },
    {
      key: "duree",
      label: "Durée",
    },
    {
      key: "type",
      label: "Catégorie"
    },
    {
      key: "location",
      label: "Salle"
    },
    {
      key: "status",
      label: "Status",
    },
    {
      key: "actions",
      label: "Action",
    }
  ]

//// LANGUAGES SUPPORTED 
export const FR = "FR";
export const EN = "EN";


export const avgEvolutionTitle = 
new Map([
    [FR, "Evolution de la moyenne d'heures d'enseignements par mois"],
    [EN, "Evolution of the average teaching hours per month"],
  ]);