export function getGroups() {
  const apiUrl = process.env.REACT_APP_API_URL + "/api/v1/group-names/all";
  var response = fetch(apiUrl).then((response) => response.json());
  return response;
}

export function getAllGroups() {
  const apiUrl = process.env.REACT_APP_API_URL + "/api/v1/events/filieres";
  var response = fetch(apiUrl).then((response) => response.json());
  return response;
}

export function addGroup(group) {
  const apiUrl = process.env.REACT_APP_API_URL + "/api/v1/group-names/add";
  var response = fetch(apiUrl, {
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    method: "POST",
    body: JSON.stringify(group),
  });
  return response;
}

export function deleteGroup(name) {
  const apiUrl = process.env.REACT_APP_API_URL + "/api/v1/group-names/delete";
  var response = fetch(apiUrl + "?name=" + name, {
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    method: "DELETE",
  });
  return response;
}

export function getGroupList() {
  const apiUrl = process.env.REACT_APP_API_URL + "/api/v1/group-names/all";
  var response = fetch(apiUrl).then((response) => response.json());
  return response;
}

export function getTeacherList() {
  const apiUrl = process.env.REACT_APP_API_URL + "/api/v1/teachers/all";
  var response = fetch(apiUrl).then((response) => response.json());
  return response;
}

export function getGroupDetails(group) {
  var response = fetch(
    process.env.REACT_APP_API_URL + "/api/v1/events/" + group
  ).then((response) => response.json());
  return response;
}

export function getTeacherDetails(firstName, lastName) {
  var response = fetch(
    process.env.REACT_APP_API_URL +
      "/api/v1/events/" +
      firstName +
      "/" +
      lastName,
    {
      headers: new Headers({
        Authorization:
          "Bearer " + JSON.parse(localStorage.getItem("cevt-user")).token,
        "Content-Type": "application/x-www-form-urlencoded",
      }),
    }
  ).then((response) => response.json());
  return response;
}

export const titleModule = new Map([
  ["PROJET DLL - TP", "PRDLL"],
  ["INITIATION A LA RECHERCHE ET PREPARATION AU MEMOIRE - TD", "PREPMEM"],
  ["FOUILLES DE DONNEES - CM", "FODO"],
  ["ARCHITECTURE DES SI - CM", "ARSI"],
  ["ARCHITECTURE ORIENTEE SERVICES - CM", "AOS"],
  ["STRATEGIE ET TI - CM", "GESO"],
  ["DEVELOPPEMENT DE LOGICIEL LIBRE - CM", "DLL"],
  ["DROIT DES CONTRATS - CM", "DROIT"],
  ["SECURITE DES SYSTEMES D INFORMATIONS", "SSI"],
  ["ARCHITECTURE DES SI - CM - SFA_M2 MIAGE CFA", "ARSI"],
  ["ARCHITECTURE ORIENTEE SERVICES", "AOS"],
  ["DEVELOPPEMENT DE LOGICIEL LIBRE", "DLL"],
  ["DROIT DES CONTRATS", "DROIT"],
  ["ANGLAIS", "ANGLAIS"],
  ["ARCHITECTURE DES SI", "ARSI"],
  ["FOUILLES DE DONNEES", "FODO"],
  ["INDEXATION ET RECHERCHE D INFORMATION", "IRI"],
  ["INITIATION A LA RECHERCHE ET PREPARATION AU MEMOIRE", "PREPMEM"],
  ["PROJET APPLICATIF 3", "PROJET3"],
  ["PROJET DLL", "DLL"],
  ["PROJET RD", "R&D"],
  ["SOUTENANCE PROJET DLL", "SOUTDLL"],
  ["SOUTENANCE PROJET ENT", "SOUTENT"],
  ["SOUTENANCE PROJET RD", "SOUTR&D"],
  ["STRATEGIE ET TI", "STI"],
  ["URBANISATION DES SI", "URBA"],
  // M1miaa
  ["ANALDON", "ANALDON"],
  ["ANALYSE DE DONNEES", "ANALDON"],
  ["ANGLAIS1", "ANGLAIS1"],
  ["ANGLAIS2", "ANGLAIS2"],
  ["BADA", "BADA"],
  ["BASE DE DONNEES APPROFONDIES", "BADA"],
  ["CONCEPTION ET PROGRAMMATION D APPLICATIONS REPARTIES", "CORBA"],
  ["COOL", "COOL"],
  ["COPROAPP", "COPROAPP"],
  ["CRYPTOGRAPHIE ET SECURITE", "CRYSEC"],
  ["CRYSEC", "CRYSEC"],
  ["DROINUM", "DROINUM"],
  ["GESFI", "GESFI"],
  ["IMP PROJET", "IMP PR"],
  ["MEFORGL", "MEFORGL"],
  ["METHODES FORMELLES POUR LE GENIE LOGICIEL", "MEFORGL"],
  ["OBJCOMPO", "OBJCOMPO"],
  ["OBJETS COMPOSANTS ET ASPECTS", "OBJCOMPO"],
  ["PROJET", "PROJET"],
  ["RECHOP", "RECHOP"],
  ["SIMGESENT", "SIMGESENT"],
  ["SIMULATION DE GESTION D ENTREPRISE", "SIMGESENT"],
  ["TECHNOLOG", "TECHNOLOG"],
  ["TECHNOLOGIES LOGICIELLES", "TECHNOLOG"],
]);

export function getTitle(value) {
  if (!titleModule.has(value)) {
    return value;
  }
  return titleModule.get(value);
}
