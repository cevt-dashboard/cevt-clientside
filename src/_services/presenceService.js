import ClientJS from "clientjs";

export function getfingerprint() {
  const windowClient = new window.ClientJS();
  const fingerPrint = windowClient.getFingerprint();

  return fingerPrint;
}
export function getMyCustomEvents(teacher) {
  const requestOptions = {
    method: "GET",

    headers: {
      "Content-Type": "application/json",
      Authorization:
        "Bearer " + JSON.parse(localStorage.getItem("cevt-user")).token,
    },
  };

  const apiUrl =
    process.env.REACT_APP_API_URL + `/api/v1/events/findmyevents/${teacher}`;
  return fetch(apiUrl, requestOptions);
}
export function closeAppel(code) {
  const requestOptions = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization:
        "Bearer " + JSON.parse(localStorage.getItem("cevt-user")).token,
    },
  };

  const apiUrl =
    process.env.REACT_APP_API_URL + `/api/v1/presence/close/${code}`;
  var response = fetch(apiUrl, requestOptions).then((response) =>
    response.json()
  );
  return response;
}

export function findTemporaryElement(code) {
  const apiUrl = process.env.REACT_APP_API_URL + `/api/v1/presence/${code}`;
  var response = fetch(apiUrl).then((response) => response.json());
  return response;
}

export function updateTemporaryElement(code, name, fingerprint) {
  const apiUrl =
    process.env.REACT_APP_API_URL +
    `/api/v1/presence/update/${code}/${name}/${fingerprint}`;
  var response = fetch(apiUrl).then((response) => response.json());
  return response;
}

export function getTemporaryElement(tmpEnseignant, tmpSeanceID, filiere) {
  const apiUrl =
    process.env.REACT_APP_API_URL +
    `/api/v1/presence/create/${tmpEnseignant}/${tmpSeanceID}/${filiere}`;
  var response = fetch(apiUrl).then((response) => response.json());
  return response;
}

export function getAllElementForEnseignant(tmpEnseignant) {
  const apiUrl =
    process.env.REACT_APP_API_URL + `/api/v1/presence/all/${tmpEnseignant}`;
  var response = fetch(apiUrl).then((response) => response.json());
  return response;
}

export function deletePresence(id) {
  const apiUrl = process.env.REACT_APP_API_URL + `/api/v1/presence/${id}`;
  const requestOptions = {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
      Authorization:
        "Bearer " + JSON.parse(localStorage.getItem("cevt-user")).token,
    },
  };

  var response = fetch(apiUrl, requestOptions);
  return response;
}

export function getAllNewsEventEvolution(date) {
  const apiUrl =
    process.env.REACT_APP_API_URL + `/api/v1/evolutions/date/${date}`;
  var response = fetch(apiUrl).then((response) => response.json());
  return response;
}
