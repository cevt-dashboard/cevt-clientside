export function getTeachersMatiereList(firstName, lastName) {
  const apiUrl =
    process.env.REACT_APP_API_URL +
    `/api/v1/events/teacher/${firstName}/${lastName}`;
  var response = fetch(apiUrl).then((response) => response.json());
  return response;
}

export function getStatsTeacherDetails(firstName, lastName) {
  const apiUrl =
    process.env.REACT_APP_API_URL +
    `/api/v1/stats/teacher/details/${firstName}/${lastName}`;
  var response = fetch(apiUrl).then((response) => response.json());
  return response;
}

export function getTeachersMatiereStats(firstName, lastName) {
  const apiUrl =
    process.env.REACT_APP_API_URL +
    `/api/v1/stats/teacher/matieres/${firstName}/${lastName}`;
  var response = fetch(apiUrl).then((response) => response.json());
  return response;
}

export function getTeachersList() {
  const apiUrl = process.env.REACT_APP_API_URL + "/api/v1/teachers/all";
  var response = fetch(apiUrl).then((response) => response.json());
  return response;
}

export function getTeachersAvancementMatiere(firstName, lastName) {
  const requestOptions = {
    method: "GET",
    headers: { "Access-Control-Allow-Origin": "*" },
  };
  const apiUrl =
    process.env.REACT_APP_API_URL +
    "/api/v1/stats/teacher/matieres/" +
    firstName +
    "/" +
    lastName;
  var response = fetch(apiUrl, requestOptions).then((response) =>
    response.json()
  );
  return response;
}

export function getTeacherDetails(firstName, lastName) {
  var response = fetch(
    process.env.REACT_APP_API_URL +
      "/api/v1/stats/teacher/details/" +
      firstName +
      "/" +
      lastName
  ).then((response) => response.json());
  return response;
}

export function getAverages() {
  var response = fetch(
    process.env.REACT_APP_API_URL + "/api/v1/stats/annual-averages"
  ).then((response) => response.json());
  return response;
}

export function getAllTeacherDetails() {
  var details = [];
  var response = getTeachersList().then(async (data) => {
    await Promise.all(
      data.map((teacher) => {
        return getTeacherDetails(teacher.firstName, teacher.lastName).then(
          (data) => {
            details.push(data);
          }
        );
      })
    );
    return details;
  });
  return response;
}

export function getMonthlyTotal() {
  var response = fetch(
    process.env.REACT_APP_API_URL + "/api/v1/stats/monthly-total"
  ).then((response) => response.json());
  return response;
}

export function addTeacher(teacher) {
  const apiUrl = process.env.REACT_APP_API_URL + "/api/v1/teachers/add";
  var response = fetch(apiUrl, {
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    method: "POST",
    body: JSON.stringify(teacher),
  });
  return response;
}

export function deleteTeacher(email) {
  const apiUrl = process.env.REACT_APP_API_URL + "/api/v1/group-names/delete";
  var response = fetch(apiUrl + "?email=" + email, {
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    method: "DELETE",
  });
  return response;
}

export function getMyCustomEvents(teacher) {
  const requestOptions = {
    method: "GET",

    headers: {
      "Content-Type": "application/json",
      Authorization:
        "Bearer " + JSON.parse(localStorage.getItem("cevt-user")).token,
    },
  };

  const apiUrl =
    process.env.REACT_APP_API_URL + `/api/v1/events/findmyevents/${teacher}`;
  return fetch(apiUrl, requestOptions);
}

export function deleteCustomSeanceTeacher(id) {
  const requestOptions = {
    method: "DELETE",

    headers: {
      "Content-Type": "application/json",
      Authorization:
        "Bearer " + JSON.parse(localStorage.getItem("cevt-user")).token,
    },
  };

  var response = fetch(
    process.env.REACT_APP_API_URL + `/api/v1/events/${id}`,
    requestOptions
  );
  return response;
}
