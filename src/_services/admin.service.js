export function getPendingEvents(type) {
  const requestOptions = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization:
        "Bearer " + JSON.parse(localStorage.getItem("cevt-user")).token,
    },
  };

  const apiUrl =
    process.env.REACT_APP_API_URL + `/admin/pending/events?valid=${type}`;
  var response = fetch(apiUrl, requestOptions).then((response) =>
    response.json()
  );
  return response;
}

export function updateEvents(body) {
  const requestOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization:
        "Bearer " + JSON.parse(localStorage.getItem("cevt-user")).token,
    },
    body: JSON.stringify(body),
  };

  const apiUrl = process.env.REACT_APP_API_URL + `/admin/add`;
  var response = fetch(apiUrl, requestOptions).then((response) =>
    response.json()
  );
  return response;
}
