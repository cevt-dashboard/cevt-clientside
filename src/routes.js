import React from 'react';


const Dashboard = React.lazy(() => import('./views/dashboard/Dashboard'));
const CalendarEnseignant = React.lazy(() => import('./views/calendarEnseignant/CalendarEnseignant'));
const Charts = React.lazy(() => import('./views/charts/Charts'));

// ENSEIGNANTS  
const EInformations = React.lazy(() => import('./views/enseignant/EInformations'));
const EPresences = React.lazy(() => import('./views/enseignant/EPresences'));



// ADMIN 
const Suivi = React.lazy(() => import('./views/admin/Suivi'));
const GestionEnseignants = React.lazy(() => import('./views/admin/GestionEnseignants.js'));


// OTHER (profile ? )
const notice = React.lazy(() => import('./views/notice/notice'));
const informations = React.lazy(() => import('./views/informations/informations'));
const Profile = React.lazy(() => import('./views/profile/Profile'));


const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/calendarEnseignant', name: 'Calendrier', component: CalendarEnseignant },
  { path: '/charts', name: 'Statistiques d\'enseignement', component: Charts },
  { path: '/profile', name: 'Profile', component: Profile },


  // ADMIN 
  { path: '/admin/gestion/suivi', name: 'Suivi', component: Suivi },
  { path: '/admin/gestion/enseignants', name: 'Gestion Enseignants', component: GestionEnseignants },


  // ENSEIGNANT
  { path: '/enseignant/EInformations', name: 'EInformations', component: EInformations },
  { path: '/enseignant/EPresences', name: 'Présences', component: EPresences },

  { path: '/notice', exact: true,  name: 'Mentions légales', component: notice },
  { path: '/informations', exact: true,  name: 'Informations', component: informations },
];

export default routes;
